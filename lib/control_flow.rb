# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  upcase_str = ""
  str.chars.each do |let|
    if let == let.upcase
      upcase_str << let
    end
  end
  upcase_str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.odd?
    return str[str.length / 2]
  else
    return str[(str.length - 1) / 2 ] + str[str.length / 2]
  end
end

# Return the number of vowels in a string.
def num_vowels(str)
  vowels = %w(a e i o u)
  counter = 0
  str.chars.each do |let|
    counter += 1 if vowels.include?(let.downcase)
  end
  counter
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
(1..num).inject(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ""
  arr.each do |el|
    result << el
    result << separator unless el == arr.last
  end
  result

end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  result = ""
  str.chars.each_index do |idx|
    if idx.odd?
      result << str[idx].upcase
    else
      result << str[idx].downcase
    end
  end
  result
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  result = []
  str.split.each do |word|
    result << word if word.length < 5
    result << word.reverse if word.length >= 5
  end
  result.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []
  (1..n).each do |num|
    if num % 3 == 0 || num % 5 == 0
      if num % 3 == 0 && num % 5 == 0
        result << "fizzbuzz"
      elsif num % 3 == 0
        result << "fizz"
      elsif num % 5 == 0
        result << "buzz"
      end
    else
      result << num
    end
  end
  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed_arr = []
  (0...arr.length).each do |idx|
    reversed_arr << arr[(arr.length - (idx + 1))]
  end
  reversed_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return true if num == 2
  arr = []
  (2...num).each do |n|
    if num % n == 0
      arr << false
    else
      arr << true
    end
  end
  return true if !arr.empty? && !arr.include?(false)
  false
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []
  (1..num).each do |n|
    result << n if num % n == 0
  end
  result
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_result = []
  result = factors(num)
  result.each do |n|
    prime_result<< n if prime?(n)
  end
  prime_result
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd_results = []
  even_results = []
  arr.each do |n|
    if n.odd?
      odd_results << n
    else
      even_results << n
    end
  end
  return odd_results[0] if odd_results.length == 1
  return even_results[0] if even_results.length == 1
end
